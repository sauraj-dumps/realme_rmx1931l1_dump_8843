#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Enable updating of APEXes
$(call inherit-product, $(SRC_TARGET_DIR)/product/updatable_apex.mk)

# Health
PRODUCT_PACKAGES += \
    android.hardware.health@2.1-impl \
    android.hardware.health@2.1-impl.recovery \
    android.hardware.health@2.1-service

# Overlays
PRODUCT_ENFORCE_RRO_TARGETS := *

# Product characteristics
PRODUCT_CHARACTERISTICS := nosdcard

# Rootdir
PRODUCT_PACKAGES += \
    init.qcom.sensors.sh \
    init.mdm.sh \
    init.qcom.sdio.sh \
    init.class_main.sh \
    init.crda.sh \
    init.qcom.class_core.sh \
    init.qti.dcvs.sh \
    init.qti.qcv.sh \
    qca6234-service.sh \
    init.qcom.post_boot.sh \
    init.qcom.efs.sync.sh \
    init.qti.chg_policy.sh \
    init.qcom.usb.sh \
    init.qcom.coex.sh \
    install-recovery.sh \
    init.qcom.sh \
    init.qcom.early_boot.sh \

PRODUCT_PACKAGES += \
    fstab.qcom \
    init.oppo.display.rc \
    init.wlan.qcom.rc \
    init.qti.ufs.rc \
    init.wlan.target.rc \
    init.qcom.rc \
    init.oppo.vendor.touchpress.rc \
    init.target.rc \
    init.qcom.factory.rc \
    init.oppo.vendor.motor.rc \
    init.qcom.usb.rc \

# Shipping API level
PRODUCT_SHIPPING_API_LEVEL := 28

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Inherit the proprietary files
$(call inherit-product, vendor/qualcomm/qssi/qssi-vendor.mk)
